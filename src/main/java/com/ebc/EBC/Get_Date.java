package com.ebc.EBC;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Get_Date {
	private static Connection Conn=null;
	private static PreparedStatement  Stmt = null;
	private static ResultSet rs=null;
	private static String Sql=null	;
	private static String[] months = {"JAN", "FEB", "MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC"};
	private static SimpleDateFormat dateFormat=new SimpleDateFormat("M");
	private static Date date = new Date();
	private static String [] [] date_Util ;
	

	public static String[][] get_Bdc_Date() {
		try {	
			Conn=Connect_db.Con_Db();
			Sql="alter session set nls_date_format = 'DD-mm-yyyy HH24:MI:SS'";
			Stmt=Conn.prepareStatement(Sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			Stmt.executeQuery();
			
			Sql="SELECT CTR_CLDR_DAY Day,CTR_CLDR_MONTH Month,CTR_CLDR_YEAR Year,V_DATE FROM Ach_Book_Calendar_test";
			Stmt=Conn.prepareStatement(Sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			rs=Stmt.executeQuery();
			rs.last();
			int nRow = rs.getRow();
			
			date_Util= new String [nRow+1][4];
			if (nRow == 0) {
				System.out.println("No Data Found");
				} else {
					rs.beforeFirst();
					int i=0;
					while(rs.next()) {
						i++;
						date_Util[i][0]=rs.getString("Day");
						date_Util[i][1]=rs.getString("Month");
						date_Util[i][2]=rs.getString("Year");
						date_Util[i][3]=months[Integer.parseInt(rs.getString("Month"))-1];
						}
				}
		
		} catch (SQLException e) {
				e.printStackTrace();
			}
			return date_Util;
	}
	
	
	
}
