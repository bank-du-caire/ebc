package com.ebc.EBC;

import com.jcraft.jsch.ChannelSftp.LsEntry;

import java.io.IOException;

public class SftpFile {
    private final LsEntry file;

    public SftpFile(LsEntry file) {
        this.file = file;
    }


    public long lastModified() throws IOException {
        return file.getAttrs().getMTime();
    }


    public boolean isDirectory() {
        return false;
    }


    public String getName() {
        return file.getFilename();
    }


    public String toString() {
        return getName();
    }
}
