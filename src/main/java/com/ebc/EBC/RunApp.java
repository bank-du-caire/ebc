package com.ebc.EBC;

import com.github.drapostolos.rdp4j.DirectoryPoller;
import com.github.drapostolos.rdp4j.spi.PolledDirectory;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

@Component
public class RunApp implements CommandLineRunner {
    TRANSACTION_FRAUD fraud = new TRANSACTION_FRAUD();
    MerchantFraud merchantFraud = new MerchantFraud();
    TRACKFILESSIZE trackfilessizeTRX = new TRACKFILESSIZE();
    TRACKFILESSIZE trackfilessizeMerch = new TRACKFILESSIZE();
    String transactionFilename = " ";
    String merchanFilename = " ";

    @Autowired
    SwiftDao swiftDao;

    @Autowired
    TRANSACTION_FRAUDREPO transactionFraudrepo;
    @Autowired
    TrackFileSizeRepo trackFileSizeRepo;
    @Autowired
    MerchantFraudREPO merchantFraudREPO;

    @PersistenceContext // or even @Autowired
    private EntityManager entityManager;
    @Value("${service.host}")
    String host;
    @Value("${service.port}")
    String port;
    @Value("${service.username}")
    String username;
    @Value("${service.password}")
    String password;
    @Value("${service.transactionFilePath}")
    String transactionFilePath;
    @Value("${service.merchantFilePath}")
    String merchantFilePath;

    @Override
    public void run(String... args) throws Exception {
        //// TEssssssssst/////****************************
        Log _logger = LogFactory.getLog(RunApp.class);
        _logger.info("Job Started....................");
        String source_file_name = "/UPG/BDC/Fraud/Transaction/20210530/TransactionFraudBanque du Caire202105308073.txt";
        String ditination_file_name = "TransactionFraudBanque du Caire202105308073.txt";
        LocalDate date = LocalDate.now().minusDays(1);
        LocalDate now = LocalDate.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");
        System.out.println("date now = " + date.format(formatter));
        connect_Ebc_Ftps Ftps = null;
        // save transaction
        File file1 = new File(transactionFilePath);
        String[] names = file1.list();
        int k = 0;
        int rows = 0;
        int rows1 = 0;
        for (String path1 : names) {
            if (new File("D:/BDC_APP/Fraud EBC/Transaction/" + path1).isDirectory()) {
                System.out.println(path1);
                File folder = new File(transactionFilePath.concat(path1));
                File[] files = new File("D:/BDC_APP/Fraud EBC/Transaction/".concat(path1)).listFiles((dir, name) -> name.endsWith(".txt"));
                EntityManager entityManager;
                System.out.println("listOfFiles.length =>> " + files.length);
                for (int i = 0; i < files.length; i++) {
                    if (files[i].isFile()) {
                        System.out.println(files[i].getName());
                    }
                    transactionFilename = files[i].getName();
                    trackfilessizeTRX.setFILENAME(transactionFilename);
                    BufferedReader reader;
                    try {
                        //fraud.setTransactionFileName(files[i].getName());
                        //Collection<TRANSACTION_FRAUD> list = transactionFraudrepo.findByTransactionFileNameAndCreationDate(files[i].getName(), now);
                        if (true) {
                            reader = new BufferedReader(new FileReader(
                                    "D:/BDC_APP/Fraud EBC/Transaction/".concat(path1).concat("/") + files[i].getName()));
                            String line = reader.readLine();
                            trackfilessizeTRX.setFOLDERNAME(path1);
                            while (line != null) {
                                String[] parts = line.split("!~");
                                //savetransactionFraudFile(parts);
                                line = reader.readLine();
                                if (parts.length > 1)
                                    rows++;
                            }
                            trackfilessizeTRX.setFILESIZE(rows);
                            rows = 0;
                            trackFileSizeRepo.save(trackfilessizeTRX);
                            reader.close();
                            File file = new File(transactionFilePath + files[i].getName());
                            //file.deleteOnExit();
                        } else {
                            System.out.println("Transaction file already inserted");
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            k++;
            //System.out.println("k = "+k);
        }
        // save marchants
        File file2 = new File(merchantFilePath);
        String[] names2 = file2.list();

        for (String path2 : names2) {
            if (new File("D:/BDC_APP/Fraud EBC/Merchant/" + path2).isDirectory()) {
                System.out.println(path2);
                File[] files1 = new File("D:/BDC_APP/Fraud EBC/Merchant/".concat(path2)).listFiles((dir, name) -> name.endsWith(".txt"));
                System.out.println("listOfFiles1.length =>> " + files1.length);
                for (int j = 0; j < files1.length; j++) {
                    if (files1[j].isFile()) {
                        System.out.println(files1[j].getName());
                    }
                    try {
                        merchanFilename = files1[j].getName();
                        trackfilessizeMerch.setFILENAME(merchanFilename);

                        //Collection<MerchantFraud> list = merchantFraudREPO.findByMerchantFileNameAndCreationDate(files1[j].getName(), now);
                        if (true) {
                            BufferedReader reader1;
                            reader1 = new BufferedReader(new FileReader(
                                    "D:/BDC_APP/Fraud EBC/Merchant/".concat(path2).concat("/") + files1[j].getName()));
                            //merchantFraud.setMerchantFileName(files1[j].getName());
                            String line1 = reader1.readLine();
                            trackfilessizeMerch.setFOLDERNAME(path2);
                            while (line1 != null) {
                                String[] parts1 = line1.split("!~");
                                //saveMerchantFraudFile(parts1);
                                line1 = reader1.readLine();
                                if (parts1.length > 1)
                                    rows1++;
                            }
                            trackfilessizeMerch.setFILESIZE(rows1);
                            rows1 = 0;
                            trackFileSizeRepo.save(trackfilessizeMerch);
                            reader1.close();
                            /*File file2 = new File(merchantFilePath + files1[j].getName());
                            file2.deleteOnExit();*/
                        } else {
                            System.out.println("Merchant file already inserted");
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    _logger.info("Update Customers Ids Succsessfuly");
                    _logger.info("Job Finnshed....................");
                    ebc_Files.export_Console("");
                    URL url = ClassLoader.getSystemResource("application.properties");
                    TripleDESUtility tdObj = new TripleDESUtility();
                }
            }
        }
    }

    public void saveMerchantFraudFile(String[] parts) throws ParseException {

        if (parts.length >= 1)
            merchantFraud.setEventAction(parts[0]);

        if (parts.length >= 2)
            merchantFraud.setMerchantType(parts[1]);

        if (parts.length >= 3)
            merchantFraud.setMerchantID(parts[2]);

        if (parts.length >= 4)
            merchantFraud.setParentMerchantID(parts[3]);

        if (parts.length >= 5)
            merchantFraud.setAcquiringMerchantID(parts[4]);

        if (parts.length >= 6)
            merchantFraud.setMerchantName(parts[5]);

        if (parts.length >= 7)
            merchantFraud.setCity(parts[6]);

        if (parts.length >= 8)
            merchantFraud.setAddress1(parts[7]);

        if (parts.length >= 9)
            merchantFraud.setAddress2(parts[8]);

        if (parts.length >= 10)
            merchantFraud.setMerchantCategoryCode(parts[9]);

        if (parts.length >= 11)
            merchantFraud.setMerchantCategoryName(parts[10]);

        if (parts.length >= 12)
            merchantFraud.setLatitude(parts[11]);

        if (parts.length >= 13)
            merchantFraud.setLongitude(parts[12]);

        if (parts.length >= 14)
            merchantFraud.setCommercialRegistrationNumber(parts[13]);

        if (parts.length >= 15)
            merchantFraud.setContactNumber(parts[14]);

        if (parts.length >= 16)
            merchantFraud.setCanRefund(parts[15]);

        if (parts.length >= 17)
            merchantFraud.setBACC(parts[16]);

        if (parts.length >= 18)
            merchantFraud.setCurrency(parts[17]);

        if (parts.length >= 19)
            merchantFraud.setIsSVA(parts[18]);

        if (parts.length >= 20)
            merchantFraud.setMinTransactionAmount(parts[19]);

        if (parts.length >= 21)
            merchantFraud.setMaxTransactionAmount(parts[20]);

        if (parts.length >= 22)
            merchantFraud.setMonthlyLimitAmount(parts[21]);

        if (parts.length >= 23)
            merchantFraud.setDailyLimitAmount(parts[22]);

        if (parts.length >= 24)
            merchantFraud.setMaxTransactioncountperday(parts[23]);

        if (parts.length >= 25)
            merchantFraud.setTahweelEnabled(parts[24]);

        if (parts.length >= 26)
            merchantFraud.setmVisaEnabled(parts[25]);

        if (parts.length >= 27)
            merchantFraud.setMeezaEnabled(parts[26]);

        if (parts.length >= 28)
            merchantFraud.setPayLinkEnabled(parts[27]);

        if (parts.length >= 29)
            merchantFraud.setEmailAddress(parts[28]);

        if (parts.length >= 30)
            merchantFraud.setTahweelID(parts[29]);

        if (parts.length >= 31)
            merchantFraud.setmVisaID(parts[30]);

        if (parts.length >= 32)
            merchantFraud.setTahweelMobileNumber(parts[31]);

        if (parts.length >= 33)
            merchantFraud.setMDRExceptional(parts[32]);

        if (parts.length >= 34)
            merchantFraud.setMDREnabled(parts[33]);

        if (parts.length >= 35) {
            if (!parts[34].equals("False") && !parts[37].equals("")) {
                Date date1 = new SimpleDateFormat("yyyy-MM-ddHH:mm:ss").parse(parts[34]);
                merchantFraud.setCreationDate(date1);
            }
        }

        if (parts.length >= 36)
            merchantFraud.setCreationUser(parts[35]);

        if (parts.length >= 37)
            merchantFraud.setApprovedBy(parts[36]);

        if (parts.length >= 38) {
            if (!parts[37].equals("False") && !parts[37].equals("")) {
                Date date1 = new SimpleDateFormat("yyyy-MM-ddHH:mm:ss").parse(parts[37]);
                merchantFraud.setLastModificationDate(date1);
            }
        }

        if (parts.length >= 39)
            merchantFraud.setNumberOfTerminals(parts[38]);

        if (parts.length >= 40)
            merchantFraud.setNumberOfBranches(parts[39]);

        if (parts.length > 0) {
            merchantFraud.setCreationDateFile(LocalDate.now());
            merchantFraud.setMerchantFileName(merchanFilename);
            merchantFraudREPO.save(merchantFraud);
        } else {
            System.out.println("Merchant file is empty");
        }

    }

    public void savetransactionFraudFile(String[] parts) throws ParseException {


        //System.out.println("Length = " + parts.length);
        if (parts.length >= 1)
            fraud.setUPG_MERCHANT_ID(parts[0]); // 004
        if (parts.length >= 2)
            fraud.setACQUIRER_ID(parts[1]); // 034556
        if (parts.length >= 3)
            fraud.setSYSTEM_TRANSACTION_ID(parts[2]);
        if (parts.length >= 4) {
            Date date1 = new SimpleDateFormat("yyyy-MM-ddHH:mm:ss").parse(parts[3]);
            fraud.setTRANSACTION_DATE(date1);
        }
        if (parts.length >= 5)
            fraud.setPAN(parts[4]);
        if (parts.length >= 6)
            fraud.setCURRENCY(parts[5]);
        if (parts.length >= 7)
            fraud.setTERMINAL_ID(parts[6]);
        if (parts.length >= 8)
            fraud.setTERMINAL_TYPE(parts[7]);
        if (parts.length >= 9)
            fraud.setAUTH_RESPONSE_CODE(parts[8]);
        if (parts.length >= 10)
            fraud.setTRANSACTION_TYPE(parts[9]);
        if (parts.length >= 11)
            fraud.setORIGINAL_TRANSACTION_ID(parts[10]);
        if (parts.length >= 12)
            fraud.setTRANSACTION_AMOUNT(parts[11]);
        if (parts.length >= 13)
            fraud.setTIPS_OR_CONV_FEES(parts[12]);
        if (parts.length >= 14)
            fraud.setMDR_VALUE(parts[13]);
        if (parts.length >= 15)
            fraud.setACQUIRER_FEES(parts[14]);
        if (parts.length >= 16)
            fraud.setCONSUMER_AMOUNT(parts[15]);
        if (parts.length >= 17)
            fraud.setTRANSACTION_NET_AMOUNT(parts[16]);
        if (parts.length >= 18) {
            fraud.setNETWORK_REFERENCE_NUMBER(parts[17]);
        }
        if (parts.length > 0) {
            fraud.setCREATIONDATE(LocalDate.now());
            fraud.setTransactionFileName(transactionFilename);
            transactionFraudrepo.save(fraud);
        } else {
            System.out.println("transaction file is empty");
        }
    }


}