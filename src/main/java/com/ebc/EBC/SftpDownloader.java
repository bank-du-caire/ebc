package com.ebc.EBC;

import com.jcraft.jsch.*;
import com.jcraft.jsch.ChannelSftp.LsEntry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.Vector;


public class SftpDownloader {
    private static final Logger logger = LoggerFactory.getLogger(SftpDownloader.class);
    private static final String tempFiles = System.getProperty("user.dir")+ File.separator+ "TempFiles";

    private Session session = null;
    private ChannelSftp channelSftp = null;

    public SftpDownloader() {

    }

    public void init(String sftpHost, String sftpPort, String sftpUser, String sftpPass) throws IOException {
        try {
            JSch jsch = new JSch();
            session = jsch.getSession(sftpUser, sftpHost, Integer.parseInt(sftpPort));
            session.setPassword(sftpPass);
            session.setConfig("StrictHostKeyChecking", "no");
            session.setServerAliveInterval(300000); // 5 mins interval
            session.setServerAliveCountMax(6); 		// Disconnects after 30 mins
            session.connect();
            Channel channel = session.openChannel("sftp");
            channel.connect();
            channelSftp = (ChannelSftp)channel;

            logger.debug("Connection to SFTP server established!!");
        } catch(JSchException ex) {
            logger.error("Error connecting to SFTP server!!", ex);
            throw new IOException(ex);
        }
    }

    @SuppressWarnings("unchecked")
    public void download(String sftpWorkingDir) throws IOException {
        logger.info("Tempfiles location: " + tempFiles);
//        Utility.createFolder(tempFiles);
        try {
            channelSftp.cd(sftpWorkingDir);
            channelSftp.lcd(tempFiles);

            Vector<LsEntry> filesList = channelSftp.ls("."); // List files in the current remote directory
            if(filesList !=null && filesList.size() > 0) {
                for(LsEntry file : filesList) {
                    if(file.getAttrs().isDir()) { // In case all files are directories, no file will be downloaded
                        continue;
                    }
                    String fileName = file.getFilename();
                    if(fileName.equals(".") || fileName.equals("..")) {
                        // skip the directory itself and parent directory
                        continue;
                    }

                    this.downloadSingleFile(fileName);
                }
            } else {
                logger.warn("There are no files to be downloaded from the remote directory in SFTP folder!!");
            }
        } catch (SftpException e) {
            logger.error("Error downloading files from SFTP server!!", e);
            throw new IOException(e);
        } catch (IOException e) {
            throw new IOException(e);
        }
    }

    private void downloadSingleFile(String fileName) throws IOException {
        try {
            channelSftp.get(fileName, fileName, null, ChannelSftp.RESUME);
            logger.debug("File '"+ fileName +"' has been downloaded successfully.");

            // Delete the file after downloading
            channelSftp.rm(fileName); // Double check not to delete the file unless it is copied first
            logger.debug("File '"+ fileName +"' has been removed from the remote directory in SFTP folder.");
        } catch (SftpException e) {
            logger.error("Error downloading file '"+fileName+"' from SFTP folder!!", e);
            throw new IOException(e);
        }
    }

    public void disconnect() {
        if (channelSftp != null && channelSftp.isConnected()) {
            channelSftp.disconnect();
        }
        if (session != null && session.isConnected()) {
            session.disconnect();
        }

        logger.debug("Connection to SFTP server terminated!!");
    }
}
