package com.ebc.EBC;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface TRANSACTION_FRAUDREPO extends JpaRepository<TRANSACTION_FRAUD,Integer> {
    @Query("SELECT u FROM TRANSACTION_FRAUD u where u.transactionFileName  = :#{#transactionFileName} and TRUNC(u.CREATIONDATE) = :#{#CREATIONDATE}")
    List<TRANSACTION_FRAUD> findByTransactionFileNameAndCreationDate(@Param("transactionFileName") String transactionFileName, @Param("CREATIONDATE") LocalDate CreationDateFile);
}


