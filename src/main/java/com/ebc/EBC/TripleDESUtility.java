package com.ebc.EBC;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.*;
import javax.crypto.spec.DESedeKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;

public class TripleDESUtility {

    private static final Logger logger = LoggerFactory.getLogger(TripleDESUtility.class);
    private static final String UNICODE_FORMAT = "UTF8";
    private static final String DESEDE_ENCRYPTION_SCHEME = "DESede";
    private KeySpec myKeySpec;
    private SecretKeyFactory mySecretKeyFactory;
    private Cipher cipher;
    private byte[] keyAsBytes;
    private String myEncryptionKey;
    private String myEncryptionScheme;
    private SecretKey key;

    public TripleDESUtility() {
        myEncryptionKey = "ThisIsSecretEncryptionKey";

        myEncryptionScheme = DESEDE_ENCRYPTION_SCHEME;

//        try {
//			keyAsBytes = myEncryptionKey.getBytes(UNICODE_FORMAT);
//		} catch (UnsupportedEncodingException e) {
//			e.printStackTrace();
//			logger.error(e.getMessage(), e);
//		}
//        try {
//			myKeySpec = new DESedeKeySpec(keyAsBytes);
//		} catch (InvalidKeyException e) {
//			e.printStackTrace();
//			logger.error(e.getMessage(), e);
//		}
//        try {
//			mySecretKeyFactory = SecretKeyFactory.getInstance(myEncryptionScheme);
//		} catch (NoSuchAlgorithmException e) {
//			e.printStackTrace();
//			logger.error(e.getMessage(), e);
//		}
//        try {
//			cipher = Cipher.getInstance(myEncryptionScheme);
//		} catch (NoSuchAlgorithmException e) {
//			e.printStackTrace();
//			logger.error(e.getMessage(), e);
//		} catch (NoSuchPaddingException e) {
//			e.printStackTrace();
//			logger.error(e.getMessage(), e);
//		}
//        try {
//			key = mySecretKeyFactory.generateSecret(myKeySpec);
//		} catch (InvalidKeySpecException e) {
//			e.printStackTrace();
//			logger.error(e.getMessage(), e);
//		}

        try {
            keyAsBytes = myEncryptionKey.getBytes(UNICODE_FORMAT);
            myKeySpec = new DESedeKeySpec(keyAsBytes);
            mySecretKeyFactory = SecretKeyFactory.getInstance(myEncryptionScheme);
            cipher = Cipher.getInstance(myEncryptionScheme);
            key = mySecretKeyFactory.generateSecret(myKeySpec);
        } catch (UnsupportedEncodingException e) {
            logger.error("Unsupported encoding format in encryption utility", e);
        } catch (InvalidKeyException e) {
            logger.error(e.getMessage(), e);
        } catch (NoSuchAlgorithmException e) {
            logger.error(e.getMessage(), e);
        } catch (NoSuchPaddingException e) {
            logger.error(e.getMessage(), e);
        } catch (InvalidKeySpecException e) {
            logger.error(e.getMessage(), e);
        }
    }

    /**
     * Method To Encrypt The String
     */
    public String encrypt(String plainString) {
        String encryptedString = "";

        try {
            cipher.init(Cipher.ENCRYPT_MODE, key);
            byte[] plainText = plainString.getBytes(UNICODE_FORMAT);
            byte[] encryptedText = cipher.doFinal(plainText);
            encryptedString = new String(Base64.encodeBase64(encryptedText));
        } catch (InvalidKeyException e) {
            logger.error(e.getMessage(), e);
        } catch (UnsupportedEncodingException e) {
            logger.error(e.getMessage(), e);
        } catch (IllegalBlockSizeException e) {
            logger.error(e.getMessage(), e);
        } catch (BadPaddingException e) {
            logger.error(e.getMessage(), e);
        }

        return encryptedString;
    }

    /**
     * Method To Decrypt An Ecrypted String
     */
    public String decrypt(String encryptedString) {
        String decryptedString = "";

        try {
            cipher.init(Cipher.DECRYPT_MODE, key);
            byte[] encryptedText = Base64.decodeBase64(encryptedString);
            byte[] plainText = cipher.doFinal(encryptedText);
            decryptedString = new String(plainText);
        } catch (InvalidKeyException e) {
            logger.error(e.getMessage(), e);
        } catch (IllegalBlockSizeException e) {
            logger.error(e.getMessage(), e);
        } catch (BadPaddingException e) {
            logger.error(e.getMessage(), e);
        }

        return decryptedString;
    }

}
