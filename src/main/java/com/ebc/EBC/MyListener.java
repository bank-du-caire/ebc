package com.ebc.EBC;

import com.github.drapostolos.rdp4j.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class MyListener implements InitialContentListener, DirectoryListener, IoErrorListener {

    private static final Logger logger = LoggerFactory.getLogger(MyListener.class);

    private SftpDownloader downloader = new SftpDownloader();

    private int cnt = 0;
    private boolean done = false;

    private String sftpHost = null;
    private String sftpPort = null;
    private String sftpUser = null;
    private String sftpPass = null;
    private String sftpWorkingDir = null;

    public MyListener(String sftpHost, String sftpPort, String sftpUser, String sftpPass, String sftpWorkingDir) {
        this.sftpHost = sftpHost;
        this.sftpPort = sftpPort;
        this.sftpUser = sftpUser;
        this.sftpPass = sftpPass;
        this.sftpWorkingDir = sftpWorkingDir;
    }

    @Override
    public void initialContent(InitialContentEvent event) {
        int filesCnt = event.getFiles().size();
        logger.debug("Initial Content in sftp server: '" +filesCnt+ "'" + " files.");
    }

    @Override
    public void fileAdded(FileAddedEvent event) {
        logger.debug("Added to sftp server: " + event.getFileElement());

        // if(event.getFileElement().getName().startsWith("Loan") || event.getFileElement().getName().startsWith("loan"))
        //	  cnt++;
        // else if(event.getFileElement().getName().startsWith("Credit") || event.getFileElement().getName().startsWith("credit"))
        //	  cnt++;


        //if(cnt == 3) {
        try {
            downloader.init(sftpHost, sftpPort, sftpUser, sftpPass);
            downloader.download(sftpWorkingDir);
            downloader.disconnect();

        } catch(IOException e) {
            downloader.disconnect();
            logger.info("Forcing the application to halt due to: "+ e.getMessage());
            System.exit(1);
        }

        done = true;
        //}
    }

    @Override
    public void fileRemoved(FileRemovedEvent event) {
        logger.debug("Removed from sftp server: " + event.getFileElement());
    }

    @Override
    public void fileModified(FileModifiedEvent event) {
        logger.debug("Modified in sftp server: " + event.getFileElement());
    }

    @Override
    public void ioErrorCeased(IoErrorCeasedEvent event) {
        logger.debug("I/O error ceased in sftp server");
    }

    @Override
    public void ioErrorRaised(IoErrorRaisedEvent event) {
        event.getIoException().printStackTrace();
        logger.error("I/O error raised in sftp server!", event.getIoException());
        logger.info("Forcing the application to halt due to: " + event.getIoException().getMessage());
        System.exit(1);
    }

    public boolean isDone() {
        return done;
    }

}
