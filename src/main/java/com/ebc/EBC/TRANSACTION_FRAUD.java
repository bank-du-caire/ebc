package com.ebc.EBC;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;

@Entity(name = "TRANSACTION_FRAUD")
public class TRANSACTION_FRAUD {

    @GeneratedValue(strategy = GenerationType.AUTO,generator = "TRANSACTION_FRAUD_SEQ")
    @Column(name = "ID")
    @Id
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUPG_MERCHANT_ID() {
        return UPG_MERCHANT_ID;
    }

    public void setUPG_MERCHANT_ID(String UPG_MERCHANT_ID) {
        this.UPG_MERCHANT_ID = UPG_MERCHANT_ID;
    }

    public String getACQUIRER_ID() {
        return ACQUIRER_ID;
    }

    public void setACQUIRER_ID(String ACQUIRER_ID) {
        this.ACQUIRER_ID = ACQUIRER_ID;
    }

    public String getSYSTEM_TRANSACTION_ID() {
        return SYSTEM_TRANSACTION_ID;
    }

    public void setSYSTEM_TRANSACTION_ID(String SYSTEM_TRANSACTION_ID) {
        this.SYSTEM_TRANSACTION_ID = SYSTEM_TRANSACTION_ID;
    }

    public Date getTRANSACTION_DATE() {
        return TRANSACTION_DATE;
    }

    public void setTRANSACTION_DATE(Date TRANSACTION_DATE) {
        this.TRANSACTION_DATE = TRANSACTION_DATE;
    }

    public String getPAN() {
        return PAN;
    }

    public void setPAN(String PAN) {
        this.PAN = PAN;
    }

    public String getCURRENCY() {
        return CURRENCY;
    }

    public void setCURRENCY(String CURRENCY) {
        this.CURRENCY = CURRENCY;
    }

    public String getTERMINAL_ID() {
        return TERMINAL_ID;
    }

    public void setTERMINAL_ID(String TERMINAL_ID) {
        this.TERMINAL_ID = TERMINAL_ID;
    }

    public String getTERMINAL_TYPE() {
        return TERMINAL_TYPE;
    }

    public void setTERMINAL_TYPE(String TERMINAL_TYPE) {
        this.TERMINAL_TYPE = TERMINAL_TYPE;
    }

    public String getAUTH_RESPONSE_CODE() {
        return AUTH_RESPONSE_CODE;
    }

    public void setAUTH_RESPONSE_CODE(String AUTH_RESPONSE_CODE) {
        this.AUTH_RESPONSE_CODE = AUTH_RESPONSE_CODE;
    }

    public String getTRANSACTION_TYPE() {
        return TRANSACTION_TYPE;
    }

    public void setTRANSACTION_TYPE(String TRANSACTION_TYPE) {
        this.TRANSACTION_TYPE = TRANSACTION_TYPE;
    }

    public String getORIGINAL_TRANSACTION_ID() {
        return ORIGINAL_TRANSACTION_ID;
    }

    public void setORIGINAL_TRANSACTION_ID(String ORIGINAL_TRANSACTION_ID) {
        this.ORIGINAL_TRANSACTION_ID = ORIGINAL_TRANSACTION_ID;
    }

    public String getTRANSACTION_AMOUNT() {
        return TRANSACTION_AMOUNT;
    }

    public void setTRANSACTION_AMOUNT(String TRANSACTION_AMOUNT) {
        this.TRANSACTION_AMOUNT = TRANSACTION_AMOUNT;
    }

    public String getTIPS_OR_CONV_FEES() {
        return TIPS_OR_CONV_FEES;
    }

    public void setTIPS_OR_CONV_FEES(String TIPS_OR_CONV_FEES) {
        this.TIPS_OR_CONV_FEES = TIPS_OR_CONV_FEES;
    }

    public String getMDR_VALUE() {
        return MDR_VALUE;
    }

    public void setMDR_VALUE(String MDR_VALUE) {
        this.MDR_VALUE = MDR_VALUE;
    }

    public String getACQUIRER_FEES() {
        return ACQUIRER_FEES;
    }

    public void setACQUIRER_FEES(String ACQUIRER_FEES) {
        this.ACQUIRER_FEES = ACQUIRER_FEES;
    }

    public String getCONSUMER_AMOUNT() {
        return CONSUMER_AMOUNT;
    }

    public void setCONSUMER_AMOUNT(String CONSUMER_AMOUNT) {
        this.CONSUMER_AMOUNT = CONSUMER_AMOUNT;
    }

    public String getTRANSACTION_NET_AMOUNT() {
        return TRANSACTION_NET_AMOUNT;
    }

    public void setTRANSACTION_NET_AMOUNT(String TRANSACTION_NET_AMOUNT) {
        this.TRANSACTION_NET_AMOUNT = TRANSACTION_NET_AMOUNT;
    }

    public String getNETWORK_REFERENCE_NUMBER() {
        return NETWORK_REFERENCE_NUMBER;
    }

    public void setNETWORK_REFERENCE_NUMBER(String NETWORK_REFERENCE_NUMBER) {
        this.NETWORK_REFERENCE_NUMBER = NETWORK_REFERENCE_NUMBER;
    }

    public LocalDate getCREATIONDATE() {
        return CREATIONDATE;
    }

    public void setCREATIONDATE(LocalDate CREATIONDATE) {
        this.CREATIONDATE = CREATIONDATE;
    }

    @Column(name = "UPG_MERCHANT_ID")
    private String UPG_MERCHANT_ID;

    @Column(name = "ACQUIRER_ID")
    private String ACQUIRER_ID;

    @Column(name = "SYSTEM_TRANSACTION_ID")
    private String SYSTEM_TRANSACTION_ID;

    @Column(name = "TRANSACTION_DATE")
    private Date TRANSACTION_DATE;

    @Column(name = "PAN")
    private String PAN;

    @Column(name = "CURRENCY")
    private String CURRENCY;

    @Column(name = "TERMINAL_ID")
    private String TERMINAL_ID;

    @Column(name = "TERMINAL_TYPE")
    private String TERMINAL_TYPE;

    @Column(name = "AUTH_RESPONSE_CODE")
    private String AUTH_RESPONSE_CODE;

    @Column(name = "TRANSACTION_TYPE")
    private String TRANSACTION_TYPE;

    @Column(name = "ORIGINAL_TRANSACTION_ID")
    private String ORIGINAL_TRANSACTION_ID;

    @Column(name = "TRANSACTION_AMOUNT")
    private String TRANSACTION_AMOUNT;

    @Column(name = "TIPS_OR_CONV_FEES")
    private String TIPS_OR_CONV_FEES;

    @Column(name = "MDR_VALUE")
    private String MDR_VALUE;

    @Column(name = "ACQUIRER_FEES")
    private String ACQUIRER_FEES ;

    @Column(name = "CONSUMER_AMOUNT")
    private String CONSUMER_AMOUNT;

    @Column(name = "TRANSACTION_NET_AMOUNT")
    private String TRANSACTION_NET_AMOUNT;

    @Column(name = "NETWORK_REFERENCE_NUMBER")
    private String NETWORK_REFERENCE_NUMBER;

    @Column(name = "CREATIONDATE")
    private LocalDate CREATIONDATE;


    public String getTransactionFileName() {
        return transactionFileName;
    }

    public void setTransactionFileName(String transactionFileName) {
        this.transactionFileName = transactionFileName;
    }

    @Column(name = "TRANSACTION_FILE_NAME")
    private String transactionFileName;
}
