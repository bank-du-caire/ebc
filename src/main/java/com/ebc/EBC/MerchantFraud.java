package com.ebc.EBC;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;
//import java.time.LocalDate;

@Entity(name = "MERCHANTFRAUD")
public class MerchantFraud {
    @GeneratedValue(strategy = GenerationType.AUTO,generator = "MERCHANTFRAUD_SEQ")
    @Column(name = "ID")
    @Id
    private Long id;

    @Column(name = "EVENTACTION")
    private String EventAction;

    @Column(name = "MERCHANTTYPE")
    private String MerchantType;

    @Column(name = "MERCHANTID")
    private String MerchantID;

    @Column(name = "PARENTMERCHANTID")
    private String ParentMerchantID;

    @Column(name = "ACQUIRINGMERCHANTID")
    private String AcquiringMerchantID;

    @Column(name = "MERCHANTNAME")
    private String MerchantName;

    @Column(name = "CITY")
    private String City;

    @Column(name = "ADDRESS1")
    private String Address1;

    @Column(name = "ADDRESS2")
    private String Address2;

    @Column(name = "MERCHANTCATEGORYCODE")
    private String MerchantCategoryCode;

    @Column(name = "MERCHANTCATEGORYNAME")
    private String MerchantCategoryName;

    @Column(name = "LATITUDE")
    private String Latitude;

    @Column(name = "LONGITUDE")
    private String Longitude;

    @Column(name = "COMMERCIALREGISTRATIONNUMBER")
    private String CommercialRegistrationNumber;

    @Column(name = "CONTACTNUMBER")
    private String ContactNumber;

    @Column(name = "CANREFUND")
    private String CanRefund;

    @Column(name = "BACC")
    private String BACC;

    @Column(name = "CURRENCY")
    private String Currency;

    @Column(name = "ISSVA")
    private String isSVA;

    @Column(name = "MINTRANSACTIONAMOUNT")
    private String MinTransactionAmount;

    @Column(name = "MAXTRANSACTIONAMOUNT")
    private String MaxTransactionAmount;

    @Column(name = "MONTHLYLIMITAMOUNT")
    private String MonthlyLimitAmount;

    @Column(name = "DAILYLIMITAMOUNT")
    private String DailyLimitAmount;

    @Column(name = "MAXTRANSACTIONCOUNTPERDAY")
    private String MaxTransactioncountperday;

    @Column(name = "TAHWEELENABLED")
    private String TahweelEnabled;

    @Column(name = "MVISAENABLED")
    private String mVisaEnabled;

    @Column(name = "MEEZAENABLED")
    private String meezaEnabled;

    @Column(name = "PAYLINKENABLED")
    private String PayLinkEnabled;

    @Column(name = "EMAILADDRESS")
    private String EmailAddress;

    @Column(name = "TAHWEELID")
    private String TahweelID;

    @Column(name = "MVISAID")
    private String mVisaID;

    @Column(name = "TAHWEELMOBILENUMBER")
    private String TahweelMobileNumber;

    @Column(name = "MDREXCEPTIONAL")
    private String MDRExceptional;

    @Column(name = "MDRENABLED")
    private String MDREnabled;

    @Column(name = "CREATIONDATE")
    private Date CreationDate;

    @Column(name = "CREATIONUSER")
    private String CreationUser;

    @Column(name = "APPROVEDBY")
    private String  ApprovedBy;

    @Column(name = "LASTMODIFICATIONDATE")
    private Date LastModificationDate;

    @Column(name = "NUMBEROFTERMINALS")
    private String NumberOfTerminals;

    @Column(name = "NUMBEROFBRANCHES")
    private String NumberOfBranches;

    @Column(name = "CREATIONDATEFILE")
    private LocalDate CreationDateFile ;


    public String getMerchantFileName() {
        return merchantFileName;
    }

    public void setMerchantFileName(String merchantFileName) {
        this.merchantFileName = merchantFileName;
    }

    @Column(name = "MERCHANT_FILE_NAME")
    private String merchantFileName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEventAction() {
        return EventAction;
    }

    public void setEventAction(String eventAction) {
        EventAction = eventAction;
    }

    public String getMerchantType() {
        return MerchantType;
    }

    public void setMerchantType(String merchantType) {
        MerchantType = merchantType;
    }

    public String getMerchantID() {
        return MerchantID;
    }

    public void setMerchantID(String merchantID) {
        MerchantID = merchantID;
    }

    public String getParentMerchantID() {
        return ParentMerchantID;
    }

    public void setParentMerchantID(String parentMerchantID) {
        ParentMerchantID = parentMerchantID;
    }

    public String getAcquiringMerchantID() {
        return AcquiringMerchantID;
    }

    public void setAcquiringMerchantID(String acquiringMerchantID) {
        AcquiringMerchantID = acquiringMerchantID;
    }

    public String getMerchantName() {
        return MerchantName;
    }

    public void setMerchantName(String merchantName) {
        MerchantName = merchantName;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getAddress1() {
        return Address1;
    }

    public void setAddress1(String address1) {
        Address1 = address1;
    }

    public String getAddress2() {
        return Address2;
    }

    public void setAddress2(String address2) {
        Address2 = address2;
    }

    public String getMerchantCategoryCode() {
        return MerchantCategoryCode;
    }

    public void setMerchantCategoryCode(String merchantCategoryCode) {
        MerchantCategoryCode = merchantCategoryCode;
    }

    public String getMerchantCategoryName() {
        return MerchantCategoryName;
    }

    public void setMerchantCategoryName(String merchantCategoryName) {
        MerchantCategoryName = merchantCategoryName;
    }

    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String latitude) {
        Latitude = latitude;
    }

    public String getLongitude() {
        return Longitude;
    }

    public void setLongitude(String longitude) {
        Longitude = longitude;
    }

    public String getCommercialRegistrationNumber() {
        return CommercialRegistrationNumber;
    }

    public void setCommercialRegistrationNumber(String commercialRegistrationNumber) {
        CommercialRegistrationNumber = commercialRegistrationNumber;
    }

    public String getContactNumber() {
        return ContactNumber;
    }

    public void setContactNumber(String contactNumber) {
        ContactNumber = contactNumber;
    }

    public String getCanRefund() {
        return CanRefund;
    }

    public void setCanRefund(String canRefund) {
        CanRefund = canRefund;
    }

    public String getBACC() {
        return BACC;
    }

    public void setBACC(String BACC) {
        this.BACC = BACC;
    }

    public String getCurrency() {
        return Currency;
    }

    public void setCurrency(String currency) {
        Currency = currency;
    }

    public String getIsSVA() {
        return isSVA;
    }

    public void setIsSVA(String isSVA) {
        this.isSVA = isSVA;
    }

    public String getMinTransactionAmount() {
        return MinTransactionAmount;
    }

    public void setMinTransactionAmount(String minTransactionAmount) {
        MinTransactionAmount = minTransactionAmount;
    }

    public String getMaxTransactionAmount() {
        return MaxTransactionAmount;
    }

    public void setMaxTransactionAmount(String maxTransactionAmount) {
        MaxTransactionAmount = maxTransactionAmount;
    }

    public String getMonthlyLimitAmount() {
        return MonthlyLimitAmount;
    }

    public void setMonthlyLimitAmount(String monthlyLimitAmount) {
        MonthlyLimitAmount = monthlyLimitAmount;
    }

    public String getDailyLimitAmount() {
        return DailyLimitAmount;
    }

    public void setDailyLimitAmount(String dailyLimitAmount) {
        DailyLimitAmount = dailyLimitAmount;
    }

    public String getMaxTransactioncountperday() {
        return MaxTransactioncountperday;
    }

    public void setMaxTransactioncountperday(String maxTransactioncountperday) {
        MaxTransactioncountperday = maxTransactioncountperday;
    }

    public String getTahweelEnabled() {
        return TahweelEnabled;
    }

    public void setTahweelEnabled(String tahweelEnabled) {
        TahweelEnabled = tahweelEnabled;
    }

    public String getmVisaEnabled() {
        return mVisaEnabled;
    }

    public void setmVisaEnabled(String mVisaEnabled) {
        this.mVisaEnabled = mVisaEnabled;
    }

    public String getMeezaEnabled() {
        return meezaEnabled;
    }

    public void setMeezaEnabled(String meezaEnabled) {
        this.meezaEnabled = meezaEnabled;
    }

    public String getPayLinkEnabled() {
        return PayLinkEnabled;
    }

    public void setPayLinkEnabled(String payLinkEnabled) {
        PayLinkEnabled = payLinkEnabled;
    }

    public String getEmailAddress() {
        return EmailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        EmailAddress = emailAddress;
    }

    public String getTahweelID() {
        return TahweelID;
    }

    public void setTahweelID(String tahweelID) {
        TahweelID = tahweelID;
    }

    public String getmVisaID() {
        return mVisaID;
    }

    public void setmVisaID(String mVisaID) {
        this.mVisaID = mVisaID;
    }

    public String getTahweelMobileNumber() {
        return TahweelMobileNumber;
    }

    public void setTahweelMobileNumber(String tahweelMobileNumber) {
        TahweelMobileNumber = tahweelMobileNumber;
    }

    public String getMDRExceptional() {
        return MDRExceptional;
    }

    public void setMDRExceptional(String MDRExceptional) {
        this.MDRExceptional = MDRExceptional;
    }

    public String getMDREnabled() {
        return MDREnabled;
    }

    public void setMDREnabled(String MDREnabled) {
        this.MDREnabled = MDREnabled;
    }

    public Date getCreationDate() {
        return CreationDate;
    }

    public void setCreationDate(Date creationDate) {
        CreationDate = creationDate;
    }

    public String getCreationUser() {
        return CreationUser;
    }

    public void setCreationUser(String creationUser) {
        CreationUser = creationUser;
    }

    public String getApprovedBy() {
        return ApprovedBy;
    }

    public void setApprovedBy(String approvedBy) {
        ApprovedBy = approvedBy;
    }

    public Date getLastModificationDate() {
        return LastModificationDate;
    }

    public void setLastModificationDate(Date lastModificationDate) {
        LastModificationDate = lastModificationDate;
    }

    public String getNumberOfTerminals() {
        return NumberOfTerminals;
    }

    public void setNumberOfTerminals(String numberOfTerminals) {
        NumberOfTerminals = numberOfTerminals;
    }

    public String getNumberOfBranches() {
        return NumberOfBranches;
    }

    public void setNumberOfBranches(String numberOfBranches) {
        NumberOfBranches = numberOfBranches;
    }

    public LocalDate getCreationDateFile() {
        return CreationDateFile;
    }

    public void setCreationDateFile(LocalDate creationDateFile) {
        CreationDateFile = creationDateFile;
    }


}
