package com.ebc.EBC;

import javax.persistence.*;
import java.util.Date;

@Entity(name = "TRACKFILESSIZE")
public class TRACKFILESSIZE {
    @GeneratedValue(strategy = GenerationType.AUTO,generator = "TRACKFILESSIZE_SEQ")
    @Column(name = "ID")
    @Id
    private Long id;

    @Column(name = "FILENAME")
    private String FILENAME;

    @Column(name = "ROW_NUMBER")
    private int FILESIZE;

    @Column(name = "FOLDERNAME")
    private String FOLDERNAME;

    public String getFILENAME() {
        return FILENAME;
    }

    public void setFILENAME(String FILENAME) {
        this.FILENAME = FILENAME;
    }

    public int getFILESIZE() {
        return FILESIZE;
    }

    public void setFILESIZE(int FILESIZE) {
        this.FILESIZE = FILESIZE;
    }

    public String getFOLDERNAME() {
        return FOLDERNAME;
    }

    public void setFOLDERNAME(String FOLDERNAME) {
        this.FOLDERNAME = FOLDERNAME;
    }





    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
