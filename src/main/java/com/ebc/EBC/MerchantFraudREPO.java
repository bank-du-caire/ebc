package com.ebc.EBC;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.Collection;
import java.util.List;

@Repository
public interface MerchantFraudREPO extends JpaRepository<MerchantFraud,Integer> {

    /*@Query("SELECT u FROM MERCHANTFRAUD u where u.CreationDateFile ")
    Collection<MerchantFraud> findAllActiveUsers();*/

    @Query("SELECT u FROM MERCHANTFRAUD u where u.merchantFileName  = :#{#merchantFileName} and TRUNC(u.CreationDateFile) = :#{#CreationDateFile}")
    List<MerchantFraud> findByMerchantFileNameAndCreationDate(@Param("merchantFileName") String merchantFileName,@Param("CreationDateFile") LocalDate CreationDateFile);
}
