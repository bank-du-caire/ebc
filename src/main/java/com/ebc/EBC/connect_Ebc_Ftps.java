package com.ebc.EBC;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.apache.commons.net.PrintCommandListener;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;
import org.apache.commons.net.ftp.FTPSClient;
import org.apache.commons.net.ftp.FTPClient;

public class connect_Ebc_Ftps {

    FTPSClient ftp = null;

    public connect_Ebc_Ftps(String host, String user, String pwd, int port_No) throws Exception {
        ftp = new FTPSClient("TLS", true);


//        ((FTPSClient) this.ftp).setTrustManager(TrustManagerUtils.getAcceptAllTrustManager());

        ftp.addProtocolCommandListener(new PrintCommandListener(new PrintWriter(System.out)));
        ftp.setStrictReplyParsing(false);
        int reply;

        ftp.connect(host, port_No);
        reply = ftp.getReplyCode();
        /*FTPFile[] files = ftp.listFiles("/UPG/BDC/Fraud/Transaction/20210523/");
        System.out.println("files.length = >>>>  "+files.length);*/

        if (!FTPReply.isPositiveCompletion(reply)) {
            ftp.disconnect();
            throw new Exception("Exception in connecting to FTP Server");
        }
//        ftp.execPBSZ(0);
        // ftp.execPROT("P");
        ftp.login(user, pwd);
        ftp.setFileType(FTP.BINARY_FILE_TYPE);
        ftp.enterLocalPassiveMode();


    }
    public String[] list_files(FTPClient ftpFile) throws Exception {
        FTPFile[] files = ftpFile.listFiles();
        System.out.println("files size = --->>> " + files.length);
        String[] fileNames = new String[files.length];
        return fileNames;
    }

    public String[] list_files(String host, String user, String pwd, int port_No) throws Exception {
        FTPClient ftpClient = new FTPClient();
        ftpClient.connect(host, port_No);
        ftpClient.login(user, pwd);
        ftpClient.setStrictReplyParsing(false);

// lists files and directories in the current working directory
        FTPFile[] files = ftpClient.listFiles();
        System.out.println("files size = --->>> " + files.length);
// iterates over the files and prints details for each
        for (FTPFile file : files) {
            String details = file.getName();
            if (file.isDirectory()) {
                details = "[" + details + "]";
            }
            details += "\t\t" + file.getSize();

            System.out.println(details);
        }

        ftpClient.logout();
        ftpClient.disconnect();

        String[] fileNames = new String[files.length];
        return fileNames;
    }
    public boolean downloadFile(String remoteFilePath, String localFilePath) {
        boolean check = false;
        try (FileOutputStream fos = new FileOutputStream(localFilePath)) {
            System.out.println("remotefile = " + remoteFilePath);
            this.ftp.retrieveFile(remoteFilePath, fos);
            int returnCode = this.ftp.getReplyCode();
            if (returnCode == 550) {
                System.out.println("returnCode = " + returnCode);
                File file = new File(localFilePath);
                //	file.deleteOnExit();
                check = false;
            } else {
                check = true;
            }
            ;
            System.out.println(returnCode);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return check;

    }

    public boolean checkFileExists(String filePath) throws IOException {
        InputStream inputStream = this.ftp.retrieveFileStream(filePath);
        int returnCode = this.ftp.getReplyCode();
        if (returnCode == 550) {
            return false;
        }
        return true;
    }

    public void disconnect() {
        if (this.ftp.isConnected()) {
            try {
                this.ftp.logout();
                this.ftp.disconnect();
            } catch (IOException f) {
                // do nothing as file is already downloaded from FTP server
            }
        }
    }


}

