package com.ebc.EBC;

import com.github.drapostolos.rdp4j.spi.FileElement;
import com.github.drapostolos.rdp4j.spi.PolledDirectory;
import com.jcraft.jsch.*;
import com.jcraft.jsch.ChannelSftp.LsEntry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.Vector;

public class SftpDirectory implements PolledDirectory {

    private static final Logger logger = LoggerFactory.getLogger(SftpDirectory.class);

    private String host;
    private String workingDirectory;
    private String username;
    private String password;

    public SftpDirectory(String host, String workingDirectory, String username, String password) {
        this.host = host;
        this.workingDirectory = workingDirectory;
        this.username = username;
        this.password = password;
    }

    public Set<FileElement> listFiles() throws IOException {
        Set<FileElement> result = new LinkedHashSet<FileElement>();
        JSch jsch = new JSch();
        Session session = null;
        ChannelSftp sftpChannel = null;
        try {
            session = jsch.getSession(username, host, 22);
            session.setConfig("StrictHostKeyChecking", "no");
            session.setPassword( password );

            session.setServerAliveInterval(300000); 	// 5 mins interval
            session.setServerAliveCountMax(6); 		// Disconnects after 30 mins
            session.connect();

            Channel channel = session.openChannel("sftp");
            channel.connect();
            sftpChannel = (ChannelSftp) channel;

            logger.debug("Connection to SFTP server established!!");

            @SuppressWarnings("unchecked")
            Vector<LsEntry> filesList = sftpChannel.ls(workingDirectory);
            for(LsEntry file : filesList) {
                result.add((FileElement) new SftpFile( file ));
            }
        } catch (JSchException e) {
            logger.error("Error connecting to SFTP server!!", e);
            throw new IOException(e);
        } catch (SftpException e) {
            logger.error("Error listing files in SFTP server!!", e);
            throw new IOException(e);
        } finally {
            if(sftpChannel != null && sftpChannel.isConnected())
                sftpChannel.exit();

            if(session != null && session.isConnected())
                session.disconnect();

            logger.debug("Connection to SFTP server terminated!!");
        }

        return result;
    }

}
