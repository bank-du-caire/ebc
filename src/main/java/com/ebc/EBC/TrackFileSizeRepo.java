package com.ebc.EBC;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TrackFileSizeRepo extends JpaRepository<TRACKFILESSIZE,Integer> {
}
