package com.ebc.EBC;

/*import org.apache.commons.collections4.map.CaseInsensitiveMap;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellValue;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;*/

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.text.StyledEditorKit.BoldAction;

public class ebc_Files {

	private static Connection Conn = null;
	private static PreparedStatement Stmt = null;
	private static ResultSet rs = null;
	private static String SQL = null;
	private static File f = new File("D://Mobpay//Mobile_Wallet_Log.txt");
	
	public static void upload_Transactions_File(String file_Path,
			String file_Name,String V_Provider_Code) throws SQLException, ClassNotFoundException,
			IOException {

		// Read an Excel File and Store in a ArrayList
		try {
			List dataHolder = Read_Transaction_File(file_Path);
	

		Conn = Connect_db.Con_Db();
//		SQL = "INSERT INTO MW_CPES_CREDIT_TRANS_TMP  VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		SQL = "INSERT INTO MW_CPES_CREDIT_TRANS_TMP(PROCESSING_DATE,COUNTRY,SOURCE_STATION,ACTION_CODE,ACTION_CODE_DESCRIPTION,LOCAL_DATE_TIME,TRANSACTION_AMOUNT,TRANSACTION_CURRENCY,PAN,\r\n" + 
				"ACCOUNT_TYPE,TO_ACCOUNT_TYPE,FROM_ACCOUNT,TO_ACCOUNT,RETREIVAL_REFERENCE_NUMBER,ACQUIRER_ID,TERMINAL_ID,TERMINAL_LOCATION,CAPTURE_DATE,\r\n" + 
				"DESTINATION_STATION,RESPONSE_CODE,RESPONSE_CODE_DESCRIPTION,AUT_NUMBER,RECEIVING_INSTITUTION_ID,SETTLEMENT_AMOUNT,SETTLEMENT_CURRENCY,\r\n" + 
				"SETTLEMENT_CONVERSION_RATE,SETTLEMENT_DATE,REMAINING_BALANCE,REVERSED,MESSAGE_TYPE_INDICATOR,MESSAGE_TYPE_IND_DESC,POS_ENTRY_MODE,\r\n" + 
				"POS_ENTRY_MODE_DESC,MERCHANT_CATEGORY_CODE,MERCHANT_ID,DESCRIPTION,TRANSACTION_SEQUENCE,LEDGER_BALANCE,CARD_EXPIRATION_DATE,RELATED_TRANSACTION,\r\n" + 
				"TRANSACTION_TIME_STAMP,SOURCE_FIID,FEES_PROFILE,TRANSIMISSION_DATE,TRACE_AUDIT_NO,FEESTYPE,CHANNEL_ID,CHANNEL_ID_DESC,ISFEES,CARDHOLDER_BILL_AMOUNT,\r\n" + 
				"CARDHOLDER_BILL_CURRENCY,CARDHOLDER_BILL_RATE,CHARGING_PROFILE,EMVSUBELEMENT71,EMVSUBELEMENT72,EMVSUBELEMENT79,EMVORIGINALPOSENTRYMODE,\r\n" + 
				"MPG_FEES,AAVRESULT,REFERENCENUMBER,SENDERNAME,SENDERADDRESS,SENDERPHONENUMBER,OPTIONALMESSAGE,POSTERMINALATTENDANCE,POSTERMINALLOCATION, \r\n" + 
				"POSCARDHOLDERPRESENCE,POSCARDPRESENCE,CARDHOLDERACTI_TERM_LEVEL,PAYMENTTRANS_TYPE,VCNVALUE,VCNEXPIRYDATE,INCONTROLCODERESULT,TRANSACTION_CLASS,\r\n" + 
				"TRANSACTION_CLASS_DESC,TRANSACTION_TYPE,TRANSACTION_TYPE_DESC,WALLET_MSISDN,WALLET_CUSTOMER_ID,WALLET_TYPE,DESTINATION_MSISDN,DESTINATION_CUSTOMER_ID,\r\n" + 
				"DESTINATION_WALLET_TYPE,PREVIOUS_BALANCE,ACH_TRANSACTION_ID,ACH_INSTRUCTION_ID,ACH_ENDTOEND_ID,ACH_CATEGORY_PURPOSE,ACH_RECEIVER_NAME,\r\n" + 
				"ACH_RECEIVER_BANK,ACH_SENDER_NAME,ACH_SENDER_BANK,ACH_SENDER_ACCOUNT_NUMBER,ACH_REMITTANCE_INFO,FILE_NAME) \r\n" +
				"VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		
		Stmt = Conn.prepareStatement(SQL);
		int count = 0;
		ArrayList cellStoreArrayList1 = null;
		// For inserting into database
		for (int i = 0; i < dataHolder.size(); i++) {
			cellStoreArrayList1 = (ArrayList) dataHolder.get(i);
			for (int r = 0; r < cellStoreArrayList1.size(); r++) {
				/*Stmt.setString(r + 1,
						((XSSFCell) cellStoreArrayList1.get(r)).toString());*/
			}
			count = Stmt.executeUpdate();
		}
		if (dataHolder.size() > 0) {

			SQL = "update MW_CPES_CREDIT_TRANS_TMP set FILE_NAME =?,PROVIDER_NO=? ";
			Stmt = Conn.prepareStatement(SQL,
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			Stmt.setString(1, file_Name);
			Stmt.setString(2, V_Provider_Code);
			Stmt.executeQuery();

			mw_Handel_Files("Transaction");
		}

		Stmt.close();
		Conn.close();
		} catch (Exception e) {
			export_Console("");
		}
	
	}

	public static void upload_REGISTRATION_File(String file_Path,
			String file_Name,String V_Provider_Code) throws SQLException, ClassNotFoundException,
			IOException {
		// Read an Excel File and Store in a ArrayList
		List dataHolder = Read_Transaction_File(file_Path);

		Conn = Connect_db.Con_Db();
		SQL = "INSERT INTO MW_REGISTRATION_TMP(CUSTOMER_ID,FIRST_NAME,MIDDLE_NAME,LAST_NAME,NAME,TITLE,BIRTH_DATE,GENDER,MARITAL_STATUS,NATIONAL_ID,SOCIAL_INSURANCE_NUMBER,HOME_ADDRESS,BUILDING_NUMBER,STREET_NAME,AREA_NAME,CITY_POLICE_NAME,POSTAL_CODE,STATEMENT_ADDRESS,LAND_LINE_NUMBER,FAX,MOBILE,SMS_MOBILE,EMAIL,ORGANIZATION,DEPARTMENT,EMPLOYEE_ID,WORK_POSITION,WORK_ADDRESS,WORK_TELEPHONE,EMPLOYMENT_DATE,FA1,FA1_EXPIRY_DATE,FA1_ALIAS,FA2,FA2_EXPIRY_DATE,FA2_ALIAS,DISTRIBUTER_CODE,DISTRIBUTER_NAME,DISTRIBUTER_BRANCH_CODE,DISTRIBUTER_BRANCH_NAME,KYC_STATUS,REGISTRATION_CHANNEL,PAN,NATIONALITY,PASSPORT_ID,WALLET_LIMIT_PROFILE,CUSTOMER_TYPE,REGISTRAR_NAME,REGISTRATIONDATE,ISDOCUMENTED,DOCUMENTSERIAL,FILE_NAME\r\n" + 
				")  VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		Stmt = Conn.prepareStatement(SQL);
		int count = 0;
		ArrayList cellStoreArrayList1 = null;
		// For inserting into database
		for (int i = 0; i < dataHolder.size(); i++) {
			cellStoreArrayList1 = (ArrayList) dataHolder.get(i);
			for (int r = 0; r < cellStoreArrayList1.size(); r++) {
				/*Stmt.setString(r + 1,
						((XSSFCell) cellStoreArrayList1.get(r)).toString());*/
			}
			count = Stmt.executeUpdate();
		}

		if (dataHolder.size() > 0) {

			SQL = "update MW_REGISTRATION_TMP set FILE_NAME =?,PROVIDER_NO=? ";
			Stmt = Conn.prepareStatement(SQL,
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			Stmt.setString(1, file_Name);
			Stmt.setString(2, V_Provider_Code);
			
			Stmt.executeQuery();

			mw_Handel_Files("REGISTRATION");

		}

		Stmt.close();
		Conn.close();
	}

	public static void upload_WalletLogReports_File(String file_Path,
			String file_Name) throws SQLException, ClassNotFoundException,
			IOException {
			ArrayList dataHolder = read_Mw_Log_File(file_Path);

		Conn = Connect_db.Con_Db();
		SQL = "INSERT INTO MW_WALLET_LOG_Tmp(LOGDATE,USERID,USERNAME,DISTRIBUTOR,DISTRIBUTORCODE,DISTRIBUTORBRANCH,DISTRIBUTORBRANCHCODE,MSISDN,FIELDNAME,OLDVALUE,NEWVALUE)  VALUES (?,?,?,?,?,?,?,?,?,?,?)";
		Stmt = Conn.prepareStatement(SQL);

		int sz = dataHolder.size();
		String line_Data;
		for (int i = 0; i < sz; i++) {
			line_Data = dataHolder.get(i).toString();
			Stmt.setString(1, line_Data.substring(0, 10).trim());
			Stmt.setString(2, line_Data.substring(10, 60).trim());
			Stmt.setString(3, line_Data.substring(60, 110).trim());
			Stmt.setString(4, line_Data.substring(110, 140).trim());
			Stmt.setString(5, line_Data.substring(140, 150).trim());
			Stmt.setString(6, line_Data.substring(150, 180).trim());
			Stmt.setString(7, line_Data.substring(180, 190).trim());
			Stmt.setString(8, line_Data.substring(190, 204).trim());
			Stmt.setString(9, line_Data.substring(204, 254).trim());
			Stmt.setString(10, line_Data.substring(254, 554).trim());
			Stmt.setString(11, line_Data.substring(554, 854).trim());
			Stmt.executeUpdate();
		}

		if (sz > 0) {

			SQL = "update MW_WALLET_LOG_Tmp set FILE_NAME =? ";
			Stmt = Conn.prepareStatement(SQL,
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			Stmt.setString(1, file_Name);
			Stmt.executeQuery();

			mw_Handel_Files("WALLET_LOG");

		}

		Stmt.close();
		Conn.close();
	}

	
	public static void upload_Consumer_Status_File(String file_Path,
			String file_Name) throws SQLException, ClassNotFoundException,
			IOException {
		// Read an Excel File and Store in a ArrayList
		List dataHolder = Read_Transaction_File(file_Path);

		Conn = Connect_db.Con_Db();
		SQL = "INSERT INTO Mw_Consumer_Status_Tmp  VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		Stmt = Conn.prepareStatement(SQL);
		int count = 0;
		ArrayList cellStoreArrayList1 = null;
		// For inserting into database
		for (int i = 0; i < dataHolder.size(); i++) {
			cellStoreArrayList1 = (ArrayList) dataHolder.get(i);
			for (int r = 0; r < cellStoreArrayList1.size(); r++) {
				/*Stmt.setString(r + 1,
						((XSSFCell) cellStoreArrayList1.get(r)).toString());*/
			}
			count = Stmt.executeUpdate();
		}

		if (dataHolder.size() > 0) {

			SQL = "update Mw_Consumer_Status_Tmp set FILE_NAME =? ";
			Stmt = Conn.prepareStatement(SQL,
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			Stmt.setString(1, file_Name);
			Stmt.executeQuery();

			mw_Handel_Files("Consumer_Status");

		}

		Stmt.close();
		Conn.close();
	}
	
	public static void mw_Handel_Files(String File_Type) throws SQLException {
		SQL = "{call  ? := MW_Handel_Files(?) }";
		CallableStatement cst = Conn.prepareCall(SQL);
		cst.registerOutParameter(1, Types.NUMERIC);
		cst.setString(2, File_Type);
		cst.executeUpdate();
		int result = cst.getInt(1);
	}

	private static List Read_Transaction_File(String file_Path)
			throws IOException {/*{
		List sheetData = new ArrayList();
		try {
			File excel = new File(file_Path);
			FileInputStream Fis = new FileInputStream(excel);
			XSSFWorkbook wb = new XSSFWorkbook(Fis);
			XSSFSheet sheet = wb.getSheetAt(0);
			int rowNum = sheet.getLastRowNum();
			int colNum = sheet.getRow(0).getLastCellNum() + 1;

			for (int i = 1; i <= rowNum; i++) {
				XSSFRow row = sheet.getRow(i);
				List data = new ArrayList();
				for (int j = 0; j < colNum; j++) {
					XSSFCell cell = row.getCell(j, Row.CREATE_NULL_AS_BLANK);
					data.add(cell);
				}
				;
				sheetData.add(data);
			}
			;
			showExelData(sheetData);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return sheetData;
	}*/
		return null;
	}

	private static void showExelData(List sheetData) {
		/*for (int i = 0; i < sheetData.size(); i++) {
			List list = (List) sheetData.get(i);
			for (int j = 0; j < list.size(); j++) {
				Cell cell = (Cell) list.get(j);
				if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
				//	System.out.print(cell.getNumericCellValue());
				} else if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
				//	System.out.print(cell.getRichStringCellValue());
				} else if (cell.getCellType() == Cell.CELL_TYPE_BOOLEAN) {
				//	System.out.print(cell.getBooleanCellValue());
				} else if (cell.getCellType() == Cell.CELL_TYPE_BLANK) {
				//	System.out.print("");
				}
				if (j < list.size() - 1) {
				//	System.out.print(",");
				}
			}
			//System.out.println();
		}*/
	};

	public static boolean check_File_Exists(String source_File_Path) {
		boolean Check;
		File f = new File(source_File_Path);
		Check = f.exists();

		return Check;
	};

	public static void export_Console(String Log_Text) throws FileNotFoundException{
		
		
		/*PrintStream Console = new PrintStream(f);
		System.setOut(Console);
		Console.print(Log_Text);*/
		
	}
	
	
	public static ArrayList read_Mw_Log_File(String File_Name) {
		String line;
		ArrayList aList = new ArrayList();
		try {
			BufferedReader input = new BufferedReader(new FileReader(File_Name));
			if (!input.ready()) {
				throw new IOException();
			}

			while ((line = input.readLine()) != null) {

				aList.add(line);

			}
		} catch (Exception e) {
			System.out.println(e);
		}

		return aList;

	}
	
	
	public static void create_AML_Reg_File(String file_Path) throws IOException
	{
		
		 String[][] Aml_File_Data= get_Reg_Data();

		 PrintWriter outputstream = new PrintWriter(file_Path);
			for (int r=1 ; r < Aml_File_Data.length ; r++) 
			{
				String Row_Data=Aml_File_Data[r][0] ;
				outputstream.println(Row_Data);
			}
			outputstream.close();
	}
	
	public static String[][] get_Reg_Data() {
		String [] [] Reg_File_Data = null ;
		try {	
			String Sql;
			Conn=Connect_db.Con_Db();
		
			
			Sql="SELECT to_char(sysdate,'yyyymmdd') || '||' || B.Cod_Cust_Id ||'||'|| trim(UPPER(A.NAME))  Data FROM MW_REGISTRATION A left join bdcefcrp.ci_custmast B on   A.NATIONAL_ID=B.COD_CUST_NATL_ID and B.FLG_MNT_STATUS='A' Where A.CUSTOMER_TYPE='Consumer' ";
			Stmt=Conn.prepareStatement(Sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			rs=Stmt.executeQuery();
			rs.last();
			int nRow = rs.getRow();
			Reg_File_Data= new String [nRow+1][4];
			
			if (nRow == 0) {
				System.out.println("No Data Found");
				} else {
					rs.beforeFirst();
					int i=0;
					while(rs.next()) {
						i++;
						Reg_File_Data[i][0]=rs.getString("Data");
						}
				}
		
		} catch (SQLException e) {
				e.printStackTrace();
			}
			return Reg_File_Data;
	}

	
	public static void update_Customer_Id() throws SQLException, ClassNotFoundException,
			IOException {
		// Read an Excel File and Store in a ArrayList

		Conn = Connect_db.Con_Db();

			SQL = "Update Army.MW_REGISTRATION A Set A.DOCUMENTSERIAL=(Select min(Ci.Cod_Cust_Id) From Bdcefcrp.ci_custmast Ci Where A.NATIONAL_ID=Ci.COD_CUST_NATL_ID and Ci.FLG_MNT_STATUS='A' )";
			Stmt = Conn.prepareStatement(SQL,
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			//Stmt.setString(1, file_Name);
			Stmt.executeQuery();


		Stmt.close();
		Conn.close();
	}

	
	
}
